@extends('layout.master')

@section('judul')
Halaman Edit Cast {{$cast->nama}}
@endsection

@section('content')

<form action="/cast/{{$cast->id}}" method="post">
  @csrf
  @method('put')
  <div class="form-group">
    <label>Nama</label>
    <input type="text" class="form-control" name="nama" value="{{$cast->nama}}">
  </div>
  @error('nama')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Umur</label>
    <input type="text" class="form-control" name="umur" value="{{$cast->umur}}">
  </div>
  @error('umur')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <div class="form-group">
    <label>Bio</label>
    <textarea class="form-control" name="bio" cols="30" rows="10">{{$cast->bio}}</textarea>
  </div>
  @error('bio')
      <div class="alert alert-danger">{{ $message }}</div>
  @enderror
  <input type="submit" class="btn btn-primary" name="kirim" value="Kirim">
</form>

@endsection