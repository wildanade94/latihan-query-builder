@extends('layout.master')

@section('judul')
Halaman Detail Cast {{$cast->nama}}
@endsection

@section('content')

<h3>Nama Cast : {{$cast->nama}}</h3>
<h3>Umur : {{$cast->umur}}</h3>
<p>Bio : {{$cast->bio}}</p>

@endsection